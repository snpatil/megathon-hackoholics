import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./styles.css";
import avatar from '../../images/g11.png';
import air from '../../images/air.png';
import rectangle from '../../images/rectangle-8.png';
import Accordion from '../../components/Accordion';
import fridge from '../../images/group-5@2x.png';
import washingMachine from '../../images/group-4@2x.png';
import microwave from '../../images/noun-microwave-743618@2x.png';

export default class BillSummary extends Component {
  constructor(props) {
    super(props);
    
  }


  componentDidMount() {
  }

  render() {
    
    return (
      <div className="container">
      <div className="app-bill-wrapper row marginLft0">
        <p className="app-bill-heading col-12">Hey Vijay, here’s your bill … </p>
        <img src={avatar} alt="test" className="col-12 avatar1"/>
        <div className="pay-bill-wrapper col-12">
          <div className="pay-bill-container col-12">
            <div className="description-text">
              <p className="total-bill">Estimated total Bill</p>
              <p className="total-bill-text">Rs 1200</p>
              <Link to="/viewdetails" className="transact-btn">View details
              </Link>
            </div>
            <div><button className="btn-pay-bill">Pay</button>
            <p className="next-bill">Next Bill</p>
            <p className="bill-date">1 Jan 2019</p></div>
          </div>
          <div className="transact-container col-12">
            <div className="transact-description">
              Buy electric units from your neighbours!
            </div>
            <Link to="/myConsumption" className="transact-btn">Transact </Link>
          </div>
      </div>
      <div className="my-appliances-section col-12">
      <div> <p className="my-appliances-text">
      My Appliances
      </p></div>
      <div> <p className="manage-text">
      MANAGE
      </p></div>
      </div>
      <div className="total-consumption-wrapper col-12">
        <div className="total-consumption-section">
        <p className="total-consumption-text">
        Total consumption
          </p>
          <p className="total-consumption-date">
          23rd Nov - Present
            </p></div>
            <div className="total-units-section">
            <p className="total-units-text">516KW</p>
            </div>
      </div>
      <div className="app-device-section col-12">
      <div className="device-left-sec">
      <img src={air} alt="test" className="app-img" />
      <p>Air Conditioner</p>
      <img src={rectangle} alt="test" className="col-12 app-img1"/>
      </div>
      <div className="device-left-right">
      <p className="device-units">176kw</p>
      </div>
      </div>
      <div className="app-device-section col-12">
      <div className="device-left-sec">
      <img src={fridge} alt="test" className="app-img" />
      <p className="device-name">Refrigerator</p>
      <img src={rectangle} alt="test" className="col-12 app-img2"/>
      </div>
      <div className="device-left-right">
      <p className="device-units">134kw</p>
      </div>
      </div>
      <div className="app-device-section col-12">
      <div className="device-left-sec">
      <img src={washingMachine} alt="test" className="app-img" />
      <p>Washing Machine</p>
      <img src={rectangle} alt="test" className="col-12 app-img3"/>
      </div>
      <div className="device-left-right">
      <p className="device-units">127kw</p>
      </div>
      </div>
      <div className="app-device-section col-12">
      <div className="device-left-sec">
      <img src={microwave} alt="test" className="app-img" />
      <p>Microwave</p>
      <img src={rectangle} alt="test" className="col-12 app-img4"/>
      </div>
      <div className="device-left-right">
      <p className="device-units">56kw</p>
      </div>
      </div>
      <div className="show-more-section col-12">
      <div className="show-more">
      Show More
      </div></div>
      
      </div>
      </div>
    )
  }
}