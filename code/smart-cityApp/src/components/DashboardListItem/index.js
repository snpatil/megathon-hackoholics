import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import  "./dashboardListItem.css";

export default class DashboardListItem extends Component {

  componentDidMount() {
    
  }
 
  render() {
    
    return (
    		<div class="row">
    		<div class="col-md-12 col-xs-12">
    	  		<div class="card-container">
    	  		  <img class="card-img-top icon" src="" alt="Card image cap" /> <label>Electricity</label>
    	  		  <div class="card-body">
    	  		  	<div class="row">
    	  		  		<div class="col-md-7">
    	  		  	       <label>Usage from 23rd Nov....</label>
    	  		  	       <div><span>Next bill on...</span></div>
    	                 <div class="row">
    	                    <div class="col-md-6">
    	                        <label>Consumption</label>
    	                        <h4>420Kg</h4>
    	                    </div>
    	                    <div class="col-md-6">
    	                      <label>Cost</label>
    	                      <h4>Rs. 4500</h4>
    	                    </div>
    	                 </div>
    	  		      </div>
    	    		    <div class="col-md-5">
    	    		        <a href="#" class="btn btn-primary">Go somewhere</a>
    	    		    </div>
    	  		    </div>
    	  		  </div>
    	  		</div>
    		</div>
    	</div>
	
    )
  }
}
