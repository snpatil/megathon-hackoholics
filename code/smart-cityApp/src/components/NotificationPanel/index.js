import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './notification.css';

export default class Notification extends Component {

  componentDidMount() {
   
  }
 
  render() {
    
    return (
    	<div class="overlay">
	<div className="clearfix notify-container">
    			<ul className="list-group">
	    		  <li className="list-group-item notify-head">
				  	<label className="pull-right"> NOTIFICATIONS</label>
				  </li>
    			  <li className="list-group-item">
    			  	<span className="notify-list"> Due date for electricity bill payment10 Dec 2018 </span>
    			  	
    			  	<span className="notify-list"> Total Amount </span><br/> <label className="metrics">Rs. 2458</label>
    			  		<br/>
    			  	<span className="notify-list"> Energy Connsumption </span><br/> <label className="metrics">256kW</label>
    			  </li>
    			  <li className="list-group-item">
    			  	<span className="notify-list"> Congratulations! you won 1000 points on your last transaction</span>
    			  </li>
    			  <li className="list-group-item">
    			  	<span className="notify-list"> Your threshold limit for the month of July exxceeded by  28kW</span>
    			  </li>
    			  <li className="list-group-item">
    			  	<span className="notify-list"> Payment successful for the monnth of Oct 2018 for LPG supply</span>
    			  </li>
    			</ul>
		</div>
	</div>

    )
  }
}
