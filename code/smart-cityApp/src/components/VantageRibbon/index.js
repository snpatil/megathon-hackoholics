import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./styles.css";
import ecoPoints from '../../images/EcoPoints.png';

export default class VantageRibbon extends Component {

  componentDidMount() {
   
  }
 
  render() {
    
    return (
        <div className="container mt-3">
            <div className="vantageRibbon">                    
                <img src={ecoPoints}/>
                <div className="ml-3">
                    <div className="ecoPointsHead">
                        My Eco points
                    </div>
                    <div className="ecoPoints">
                        50,000
                    </div>
                </div>                    
            </div>
        </div>

    )
  }
}