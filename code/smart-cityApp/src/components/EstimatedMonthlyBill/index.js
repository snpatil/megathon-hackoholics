import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./styles.css";

export default class EstimatedMonthlyBill extends Component {

  componentDidMount() {
   
  }
 
  render() { 
    return (
        <div className="container mt-3">
        <div className="monthlyBillRow">
          <h3>November</h3>
          <a href="#">Download</a>
        </div>
        <div className="monthlyBillContainer">
            <div className="row mb-3">
              <div className="col">
                <div>Estimated Total Bill</div>
                <strong>Rs. 1500</strong>
              </div>
              <div className="col">
                <div>Estimated Consumption</div>
                <strong>1200 KW</strong>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <div>Next bill on</div>
                <strong>01 Jan'18</strong>
              </div>
              <div className="col">
                <div>Cost per unit</div>
                <strong>Rs.12/unit</strong>
              </div>
            </div>
            
        </div>
      </div>

    )
  }
}