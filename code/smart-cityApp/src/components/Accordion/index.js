import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./styles.css";
import infoIcon from '../../images/infoIcon.png';
import modalInfoIcon from '../../images/modalInfoIcon.png';
import closeModal from '../../images/closeModal.png';
import fridge from '../../images/group-5.png';
import washingMachine from '../../images/group-4.png';
import microwave from '../../images/noun-microwave-743618.png';


export default class AccordionImp extends Component {
  constructor(props) {
    super(props);
    
  }


  componentDidMount() {
  }

  render() {
    
    return (
        <div className="container mt-5 mb-3">        
        <div id="accordion" className="accordionsContainer">
          <div className="card">
            <div className="card-header">
              <a className="collapsed buttonImage card-link d-block" data-toggle="collapse" href="#collapseOne">              
                Current Consumption                
              </a>
            </div>
            <div id="collapseOne" className="collapse" data-parent="#accordion">
              <div className="card-body">
                <div className="accordionDetailsCntnr m-1 p3 row no-gutters">
                  <div className="col-10">Tariff</div>
                  <div className="col-2">XXX</div>
                  <div className="col-10">Meter Number</div>
                  <div className="col-2">123456789</div>
                  <div className="col-10">Multiplying Factor</div>
                  <div className="col-2">XX</div>
                  <div className="col-10">Present Reading</div>
                  <div className="col-2">XX</div>
                  <div className="col-10">Previous Reading</div>
                  <div className="col-2">XX</div>
                  <div className="col-10">Consumption</div>
                  <div className="col-2">XX</div>
                  <div className="col-10">Energy Charge</div>
                  <div className="col-2">Rs. XX</div>
                  <div className="col-10 mb-0">Fixed Charge</div>
                  <div className="col-2 mb-0">Rs. XX</div>

                </div>
              </div>
            </div>
          </div>
          <div className="card">
            <div className="card-header">
              <a className="buttonImage collapsed card-link d-block" data-toggle="collapse" href="#collapseTwo">              
                Appliance Consumption                
            </a>
            </div>
            <div id="collapseTwo" className="collapse" data-parent="#accordion">
              <div className="card-body">
                <div className="row appliance">
                  <div className="col-md-12">
                    <img src={washingMachine} alt="test"/>
                    <p className="accordDevice mt-2">Washing Machine | 123456</p>
                    <div className="description-container-wrapper col-6">
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Consumption
                        </div>
                        <strong>
                        234 kw
                        </strong>
                      </div>
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Cost
                        </div>
                        <strong>
                        Rs. 643
                        </strong>
                      </div>
                    </div>
                    </div>
                </div>

<div className="row appliance">
                  <div className="col-md-12">
                    <img src={fridge} alt="test"/>
                    <p className="accordDevice mt-2">Refrigerator | 123456</p>
                    <div className="description-container-wrapper col-6">
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Consumption
                        </div>
                        <strong>
                        134 kw
                        </strong>
                      </div>
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Cost
                        </div>
                        <strong>
                        Rs. 467
                        </strong>
                      </div>
                    </div>
                    </div>
                </div>

                <div className="row appliance">
                  <div className="col-md-12">
                    <img src={microwave} alt="test"/>
                    <p className="accordDevice mt-2">Air Conditioner | 123456</p>
                    <div className="description-container-wrapper col-6">
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Consumption
                        </div>
                        <strong>
                        234 kw
                        </strong>
                      </div>
                      <div className="description-container">
                        <div className="costConsumeTxt">
                        Cost
                        </div>
                        <strong>
                        Rs. 643
                        </strong>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div className="card">
            <div className="card-header">
              <a className="buttonImage collapsed card-link d-block" data-toggle="collapse" href="#collapseThree">              
                Bill Calculation
              </a>
            </div>
            <div id="collapseThree" className="collapse" data-parent="#accordion">
              <div className="card-body">
                <div className="billCalculationCntnr m-1 p3 row no-gutters">
                  <div className="col-9">Fixed Charge</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Meter Number</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Multiplying Factor</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Present Reading</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Previous Reading</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Consumption</div>

<div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9">Energy Charge</div>
                  <div className="col-2">Rs. xx</div>
                  <div className="col-1 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                  <div className="col-9 mb-0">Fixed Charge</div>
                  <div className="col-2 mb-0">Rs. xx</div>
                  <div className="col-1 mb-0 text-right"><button type="button" data-toggle="modal" data-target="#billInfoModal"><img className="infoIcon" src={infoIcon}/></button></div>
                </div>
              </div>
            </div>
          </div>
        </div>

        
        
        <div class="modal" id="billInfoModal">
          <div class="modal-dialog">
            <div class="modal-content">

              
              <div class="modal-header">
                <button type="button" className="btn btn-link close" data-dismiss="modal"><img className="closeModal" src={closeModal}/></button>
              </div>

              
              <div class="modal-body">
              <h4 class="modal-title"><img className="modalInfoIcon" src={modalInfoIcon}/>Regulatory Asset Charges</h4>                
              Regulatory Asset Charger is the Charge for past recoveries of the Distribution Utility which could not be recovered since, the approved tariff was not sufficient to recoverthe costs incurred.
              </div>

            </div>
          </div>
        </div>
      </div>
    )
  }
}