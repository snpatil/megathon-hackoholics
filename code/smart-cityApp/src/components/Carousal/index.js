import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import VantageRibbon from '../../components/VantageRibbon';
import './carousal.css';
import { Link } from 'react-router-dom';

export default class Carousel extends Component {
    render() {
    	return (
    			<div id="carouselExampleIndicators" data-interval="false" className="carousel slide" data-ride="carousel">
    			  <ol className="carousel-indicators">
    			    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
    			    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    			    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    			  </ol>
    			  <div className="carousel-inner">


						  <div className="carousel-item active first">
							  <div className="topbar">
									<div>
										<div className="light-text">My Eco points</div>
										<div className="points-text">50,000</div>
									</div>
							  </div>
							  <Link to="/electricity">
							  <div className="firstItem">

								  <div className="electricity">
							  		</div>

							  </div>
							  </Link>
						  </div>


    			    <div className="carousel-item second">
						<div className="topbar">
							<div>
								<div className="light-text">My Eco points</div>
								<div className="points-text">50,000</div>
							</div>
						</div>

							<div className="secondItem">

								<div className="electricity">
								</div>

							</div>
    			    </div>
    			    <div className="carousel-item third">
						<div className="topbar">
							<div>
								<div className="light-text">My Eco points</div>
								<div className="points-text">50,000</div>
							</div>
						</div>

						<div className="thirdItem">

							<div className="electricity">
							</div>

						</div>
    			    </div>
    			  </div>
    			  <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    			    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    			    <span className="sr-only">Previous</span>
    			  </a>
    			  <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    			    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    			    <span className="sr-only">Next</span>
    			  </a>
    			</div>
    	)
    }
}