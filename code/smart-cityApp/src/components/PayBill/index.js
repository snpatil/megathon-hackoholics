import React from 'react';
import "./styles.css";

export default function PayBill() {
	return (
		<div>
            <div className="pay-bill-wrapper row">
                <div className="pay-bill-container col-12">
                    <div className="description-text col-8">
                    <p>Total Bill</p>
                    <p>Rs 1200</p>
                    <a href="#"> View bill details </a>
                    </div>
                    <div className="col-4"><button className="btn-pay-bill">Pay Bill</button></div>
                </div>
                <div className="transact-container col-12">
                    <div className="transact-description col-9">
                    Buy electric units from your neighbours!
                    </div>
                    <div className="transact-btn col-3"><a href="#"> Transact </a></div>
                </div>
            </div>
		</div>
	)
}