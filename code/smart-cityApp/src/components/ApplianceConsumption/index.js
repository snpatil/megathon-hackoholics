import React from 'react';
import "./styles.css";

export default function ApplianceConsumption() {
	return (
		<div>

        <div className="appliance-consumption row">
        <div className="appliance-container col-12">
            <div className="left-section col-1">
            <img
                className="appliance-image"
                height="50"
                width="50"
                src="https://pbs.twimg.com/profile_images/1138119813/-9_400x400.jpg"
            />
            </div>
            <div className="appliance-info col-3">
            <p className="app-heading">Refrigerator</p>
            <p className="app-desc">#123456</p>
            </div>
            <div className="appliance-consumption-text col-3">
            <p className="app-heading">cost</p>
            <p className="app-desc">Rs 147</p>
            </div>
            <div className="appliance-consumption-cost col-3">
            <p className="app-heading">Consumption</p>
            <p className="app-desc">135kw</p>
            </div>
            <div className="appliance-btn col-2">
            <div>
                <label className="switch">
                <input type="checkbox"/> <span className="slider round"></span>
                </label>
            </div>

            <a href="#">Remove</a>
            </div>
        </div>
        </div>


		</div>
	)
}