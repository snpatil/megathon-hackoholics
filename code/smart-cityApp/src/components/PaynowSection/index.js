import React from 'react';
import "./pauSection.css";

export default function PayNowSection() {
	return (
			<div className="col-md-12 greyBg pt-2 pb-2 mt-3">
				<div className="row mb-2">
					<div className="col-sm-2">
						<div className="pl-4">&lt;</div>
						</div>
					<div className="col-sm-8 text-center">
						November 2018
					</div>
					<div className="col-sm-2 text-right">
						<div className="pr-4">&gt;</div></div>
				</div>
				<div className="row">
					<div className="col-sm-4">
						<div className="pl-4">
							<div>
								Total Amount Due
							</div>
							<h6>
								Rs. 1500
							</h6>
						</div>
					</div>
					<div className="col-sm-4">
						<div>
							Production
						</div>
						<h6>
							1000 KW
						</h6>
					</div>
					<div className="col-sm-4">
						<div>
							Consumption
						</div>
						<h6>
							12000 KW
						</h6>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<input type="button" value="Pay Now" className="btn-primary btn"></input>
						<a href="#" className="ml-4">View Bill</a>
					</div>
					<div className="col-sm-6 text-right">
						<a href="#" className="ml-4">Purchase Book</a>
					</div>
				</div>
			</div>
	)
}