import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "./styles.css";
import Switch from "react-switch";

export default class DeviceConnected extends Component {
  constructor(props) {
    super(props);
    this.state = {checked: false,connectedDevices:[]};
    //this.switchChange = this.switchChange.bind(this);
  }

  switchChange(e,index) {
      let {connectedDevices} = this.state;
	  connectedDevices[index].enabled = !connectedDevices[index].enabled;
	  this.setState({
		  connectedDevices
	  });
  }

  componentDidMount() {
    fetch('https://smartcityenergy.firebaseapp.com/getConnectedDevices')
      .then(response => response.json())
      .then(data => this.setState(
        { 
        connectedDevices: data.devices }
        ));
  }
 

  render() {
    
    return (
      <div className="deviceConnected mt-3 p-4">
        <div className="container">
          <h5 className="mb-3"><strong>Devices Connected</strong></h5>
        {this.state.connectedDevices.map((device,index) => (
          <div className="deviceRow mb-3">
            <div className="pt-2">
            <span><img className="icons" src={device.imgUrl} /></span>
            <span className="ml-3">
              {device.name}
            </span>
          </div>
        <div>
        <label htmlFor={device.name}>
        <Switch
          onChange={(e)=>{
            this.switchChange(this,index);
          }}
          checked={device.enabled}
          id={device.name}
          onColor="#7ED321"
          offHandleColor="#FFF"
          onHandleColor="#FFF"
          handleDiameter={10}
          uncheckedIcon={false}
          checkedIcon={false}
          height={20}
          width={48}
        />
      </label>
      </div>
      </div>
        ))}     
      </div></div>
    )
  }
}