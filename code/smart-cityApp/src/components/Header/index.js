import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../images/fill-1.png';
import hamburger from '../../images/group.png';
import notification from '../../images/notification.png';
import document from '../../images/document.png';
import group from '../../images/group-11.png';
import customs from '../../images/customer-support-2.png';
import user from '../../images/user.png';
import support from '../../images/support.png';
import NotificationPanel from '../NotificationPanel';
import "./header.css";

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true,isNotificationOn: false};

    this.toggleNav = this.toggleNav.bind(this);
    this.handleNotificationIconClick = this.handleNotificationIconClick.bind(this);
  }

  toggleNav() {
    console.log('state',this.state.isToggleOn)
    this.setState({
      isToggleOn: !this.state.isToggleOn
    });    
  }
  handleNotificationIconClick(){
      console.log('state notification',this.state.isNotificationOn)
  	  this.setState({
    	  isNotificationOn: !this.state.isNotificationOn
    	})
     }
  componentDidMount() {

  }
 

  render() {
    
    return (

    <div>
      <div id="mySidenav" className={"sidenav "+ (this.state.isToggleOn ? 'slideIn' : 'slideOut')}>        
        <div className="nav-section row">
        <div className="col-12 profile-img"> <img src={user} /></div>
        <div className="col-12 profile-txt"><p className="profile-name">Vijay Joshi</p></div>
        </div>
        <div className="nav-sub-section row">
        <div className="col-12 profile-img"> <img src={document}/></div>
        <div className="col-12 profile-txt"><p className="profile-doc-text">My Documents</p></div>
        </div>
        <div className="nav-sub-section row">
        <div className="col-12 profile-img"> <img src={group}/></div>
        <div className="col-12 profile-txt"><p className="profile-doc-text">My Transactions</p></div>
        </div>
        <div className="nav-sub-section row">
        <div className="col-12 profile-img"> <img src={customs}/></div>
        <div className="col-12 profile-txt"><p className="profile-doc-text">Customize Dashboard</p></div>
        </div>
        <div className="nav-sub-section row">
        <div className="col-12 profile-img"> <img src={support}/></div>
        <div className="col-12 profile-txt"><p className="profile-doc-text">Help</p></div>
        </div>
      </div>

      <div className={"main header "+ (this.state.isToggleOn ? 'mainSlideOut' : 'mainSlideIn')}>
      <div className="d-flex">
          <div className="align-self-center">
            <img src={hamburger} className={"cursor-pointer "+ (this.state.isToggleOn ? 'mainSlideOut' : 'mainSlideIn')} onClick={this.toggleNav}/>
          </div>
          <div className="text-center align-self-center">
          <Link to="/"><img src={logo}/>
          </Link>
          </div>
          <div className="text-right align-self-center">
            <img src={notification} onClick={this.handleNotificationIconClick}/>
          </div>
        </div>

      </div>
      {this.state.isNotificationOn && <NotificationPanel />}
    </div>
    )
  }
}