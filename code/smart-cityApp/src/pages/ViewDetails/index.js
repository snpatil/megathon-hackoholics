import React, { Component } from 'react';
import AccordionImp from '../../components/Accordion';
import Header from '../../components/Header';
import EstimatedMonthlyBill from '../../components/EstimatedMonthlyBill';

export default class ViewDetails extends Component {

  componentDidMount() {
  
  }
  render() {
	
    return (
    		
            <div>
            <Header />

            <EstimatedMonthlyBill />
            <AccordionImp />
            </div>
            

    )
  }
}
