import React, { Component } from 'react';
import BillSummary from '../../components/BillSummary';
import Header from '../../components/Header';
import './energySavings.css';
import avatar from '../../images/g11.png';
import air from '../../images/air.png';
import rectangleOrange from '../../images/rectangle-orange.png';
import rectangleGreen from '../../images/rectangle-green.png';
import fridge from '../../images/group-5@2x.png';
import washingMachine from '../../images/group-4@2x.png';
import microwave from '../../images/noun-microwave-743618@2x.png';
import Switch from "react-switch";

export default class MyEnergySavings extends Component {

	componentDidMount() {

	}
	render() {

		return (

			<div>
				<Header />
				<div className="row">

					<div className="col-12 titleTxt ml-3 mt-3 mb-4">
						My Energy savings

					</div>
				</div>
				<div className="row mb-4">
					<div className="col-5 dateTxt ml-3">
						Normal Settings
						<img src={rectangleOrange} alt="test" />
					</div>
					<div className="col-6 priceTxt  text-right">
						<span className="priceTxt greyFont">420</span>
						<span className="unit greyFont">KW</span>
					</div>
				</div>
				<div className="row mb-4">
					<div className="col-5 dateTxt ml-3">
						Eco Settings
						<img src={rectangleGreen} alt="test" />
					</div>
					<div className="col-6 priceTxt  text-right">
						<span className="priceTxt greenFont">420</span>
						<span className="unit greenFont">KW</span>
					</div>
				</div>
				<div className="row mb-3">
					<div className="col-8 addIcon">
						<span className="addApplianceTxt">Add Appliance</span>
					</div>
					<div className="col-3 editTxt  text-right">
						Edit
					</div>
				</div>
				<div className="myApplianceBg col-12 mt-3">
					<div className="col-12 mt-3 border-btm pb-3">
						<div className="row">
							<div className="col-8">
								<img src={air} alt="test" className="app-img" />
								<p className="device-name">Air Conditioner</p>
							</div>
							<div className="col-4 text-right">
								<Switch checked={true} onColor="#7ED321"
										offHandleColor="#FFF"
										onHandleColor="#FFF"
										handleDiameter={10}
										uncheckedIcon={false}
										checkedIcon={false}
										height={20}
										width={48} />
							</div>
						</div>



					</div>
					<div className="col-12 mt-3 border-btm pb-3">
						<div className="row">
							<div className="col-8">
								<img src={fridge} alt="test" className="app-img" />
								<p className="device-name">Refrigerator</p>
							</div>
							<div className="col-4 text-right">
								<Switch checked={true} onColor="#7ED321"
										offHandleColor="#FFF"
										onHandleColor="#FFF"
										handleDiameter={10}
										uncheckedIcon={false}
										checkedIcon={false}
										height={20}
										width={48} />
							</div>
						</div>



					</div>
					<div className="col-12 mt-3 border-btm pb-3">
						<div className="row">
							<div className="col-8">
								<img src={washingMachine} alt="test" className="app-img" />
								<p className="device-name">Washing Machine</p>
							</div>
							<div className="col-4 text-right">
								<Switch checked={true} onColor="#7ED321"
										offHandleColor="#FFF"
										onHandleColor="#FFF"
										handleDiameter={10}
										uncheckedIcon={false}
										checkedIcon={false}
										height={20}
										width={48} />
							</div>
						</div>



					</div>
					<div className="col-12 mt-3 border-btm pb-3">
						<div className="row">
							<div className="col-8">
								<img src={microwave} alt="test" className="app-img" />
								<p className="device-name">Microwave</p>
							</div>
							<div className="col-4 text-right">
								<Switch checked={true} onColor="#7ED321"
										offHandleColor="#FFF"
										onHandleColor="#FFF"
										handleDiameter={10}
										uncheckedIcon={false}
										checkedIcon={false}
										height={20}
										width={48} />
							</div>
						</div>



					</div>
				</div>

			</div>


		)
	}
}
