import React, { Component } from 'react';

import Carousal from "../../components/Carousal"
import Header from '../../components/Header';
import DeviceConnected from "../../components/DeviceConnected";
import BillSummary from "../../components/BillSummary"
import  "./dashboard.css";

export default class Dashboard extends Component {

  componentDidMount() {
    

  }

  render() {
    
    return (
        <div className="section-container">
            <div className="parent">
                <Header/>

                <div className = "row renderCarousal">
                    <div className="col-md-12">
                        <Carousal />
                    </div>
                </div>
            </div>

            <DeviceConnected />


        </div>

    )
  }
}
