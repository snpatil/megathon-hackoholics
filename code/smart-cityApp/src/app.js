import React, { Component } from "react";
import {BrowserRouter,Switch,Route} from "react-router-dom";
import Dashboard from './pages/Dashboard';
import Electricity from './pages/Electricity';
import ViewDetails from './pages/ViewDetails';
import MyApplianceConsumption from './pages/MyApplianceConsumption';
import Header from './components/Header';
import MyEnergySavings from "./pages/MyEnergySavings/index";

const f0f = () => (<span>PAGE NOT FOUND</span>);

class App extends Component{
	render(){
		return (
			<div className="app">
				<BrowserRouter>
					<Switch>
						<Route exact path="/" render={()=> <Dashboard />} />
						<Route path="/electricity" render={()=> <Electricity />} />
						<Route path="/viewDetails" render={()=> <ViewDetails />} />
						<Route path="/myConsumption" render={()=> <MyApplianceConsumption />} />
						<Route path="/myEnergySavings" render={()=> <MyEnergySavings />} />
						<Route component={f0f} />
					</Switch>
				</BrowserRouter>
			</div>
		);
	}
}

export default App;