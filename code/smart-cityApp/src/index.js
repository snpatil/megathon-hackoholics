import React from "react";
import ReactDOM from "react-dom";
import { hot } from 'react-hot-loader';
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import  '../src/common/styles.css';
import App from "./app";

const AppWithHot = hot(module)(App);

var mountNode = document.getElementById("app");
ReactDOM.render(<AppWithHot />, mountNode);