const functions = require('firebase-functions');
const admin = require('firebase-admin');
let express = require('express');
const cors = require('cors');
var logger = require("./logger.js");

// Fetch the service account key JSON file contents
var serviceAccount = require("./smartcityenergy-417c1e518d53.json");

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://smartcityenergy.firebaseio.com"
});

let app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get("/home",(req,res)=>{
	res.send("HEllo from cloud functions");
});

app.get("/getUsers",(req,res)=>{
	//res.setHeader('Content-Type', 'application/json');
	let users = [];

	try{
		let db = admin.database();
		var ref = db.ref("/users");

		ref.on("child_added", (dataSnapshot)=>{
			let name,email;
			dataSnapshot.forEach((data) => {

				if(data.key === "email"){
					email = data.val();
				}

				if(data.key === "name"){
					name = data.val();
				}




			});

			users.push({
				"name":name,
				"email":email
			});


		});
		res.status(200).end(JSON.stringify({users:users}));

	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:"Error Occured while fetching results."}));
	}

});

app.get("/getUserDetails",(req,res)=>{
	res.setHeader('Content-Type', 'application/json');

	try{
		let db = admin.database();
		var ref = db.ref("/User_Details");

		let userDetails;
		var userDetailsRef = ref.once('value').then((userDetailsSnapshot) => {
			userDetailsSnapshot.forEach((userDetailSnapshot) => {
				let userId = userDetailSnapshot.key;
			});

			userDetails = JSON.parse(JSON.stringify(userDetailsSnapshot.val()));
			let userDetailsArr = Object.keys(userDetails).map(i => userDetails[i]);
			logger.info("************* Inside get user Details try *********************");
			logger.info(userDetails);
			res.status(200).end(JSON.stringify({userDetails:userDetailsArr}));

			return userDetails;

		}).catch((error)=>{
			logger.error("************* Inside get user Details catch *********************");
			throw new Error("Error Occured while fetching results.")
		});


	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:e}));
	}

});

app.get("/getConnectedDevices",(req,res)=>{
	res.setHeader('Content-Type', 'application/json');

	try{
		let db = admin.database();
		let ref = db.ref("/Connected_Devices");
		let devices;
		var deviceRef = ref.once('value').then((connectedDevicesSnapshot) => {
			connectedDevicesSnapshot.forEach((connectedDeviceSnapshot) => {
				let deviceId = connectedDeviceSnapshot.key;
			});

			devices = JSON.parse(JSON.stringify(connectedDevicesSnapshot.val()));
			let devicesArr = Object.keys(devices).map(i => devices[i]);
			logger.info("************* Inside get connected Devices try *********************");
			logger.info(devices);
			res.status(200).end(JSON.stringify({devices:devicesArr}));

			return devices;

		}).catch((error)=>{
			logger.error("************* Inside get connected Devices catch *********************");
			throw new Error("Error Occured while fetching results.")
		});
	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:e}));
	}
});

app.get("/getEnergyResources",(req,res)=>{
	res.setHeader('Content-Type', 'application/json');

	try{
		let db = admin.database();
		let ref = db.ref("/Energy_Resources");
		let energyRes;
		let energyResRef = ref.once('value').then((energyResSnapshot) => {
			energyResSnapshot.forEach((energySnapshot) => {
				let energyResId = energySnapshot.key;
			});

			energyRes = JSON.parse(JSON.stringify(energyResSnapshot.val()));
			let energyResArr = Object.keys(energyRes).map(i => energyRes[i]);
			logger.info("************* Inside get Energy_Resources try *********************");
			logger.info(energyRes);
			res.status(200).end(JSON.stringify({energyResources:energyResArr}));

			return energyRes;

		}).catch((error)=>{

			logger.error("************* Inside get Energy_Resources catch *********************");
			throw new Error("Error Occured while fetching results.")
		});
	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:e}));
	}
});

app.get("/getNotification",(req,res)=>{
	res.setHeader('Content-Type', 'application/json');

	try{
		let db = admin.database();
		let ref = db.ref("/Notifications");
		let notifications;
		let notificationRef = ref.once('value').then((notificationsSnapshot) => {
			notificationsSnapshot.forEach((notificationSnapshot) => {
				let notificationId = notificationSnapshot.key;
			});

			notifications = JSON.parse(JSON.stringify(notificationsSnapshot.val()));
			let notificationsArr = notifications && Object.keys(notifications).map(i => notifications[i]);
			logger.info("************* Inside get Notification try *********************");
			logger.info(notifications);
			res.status(200).end(JSON.stringify({notifications:notificationsArr}));

			return notifications;

		}).catch((error)=>{
			logger.error("************* Inside get connected Devices catch *********************");
			throw new Error("Error Occured while fetching results.")
		});
	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:e}));
	}
});

app.get("/getAppliances",(req,res)=>{
	res.setHeader('Content-Type', 'application/json');

	try{
		let db = admin.database();
		let ref = db.ref("/Appliances");
		let appliances;
		let appliancesRef = ref.once('value').then((appliancesSnapshot) => {
			appliancesSnapshot.forEach((applianceSnapshot) => {
				let applianceId = applianceSnapshot.key;
			});

			appliances = JSON.parse(JSON.stringify(appliancesSnapshot.val()));
			let appliancesArr = appliances && Object.keys(appliances).map(i => appliances[i]);
			logger.info("************* Inside get appliances try *********************");
			logger.info(appliances);
			res.status(200).end(JSON.stringify({appliances:appliancesArr}));

			return appliances;

		}).catch((error)=>{
			logger.error("************* Inside get appliances catch *********************");
			throw new Error("Error Occured while fetching results.")
		});
	}catch(e){
		res.status(500).end(JSON.stringify({errMsg:e}));
	}
});

exports.app = functions.https.onRequest(app);

